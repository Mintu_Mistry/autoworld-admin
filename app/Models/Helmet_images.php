<?php  

namespace App\Models;

use CodeIgniter\Model;

class Helmet_images extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('helmet_images');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($helmet_image_id = '')
	{	
		if($helmet_image_id){
			$this->builder->where("helmet_image_id",$helmet_image_id );
			return $this->builder->get()->getRowArray();
		}
		else {
			$this->builder->orderBy('helmet_image_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $helmet_image_id )
	{	
		$this->builder->where("helmet_image_id",$helmet_image_id );
		$this->builder->update($data);
	}

	public function crud_delete($helmet_image_id)
	{	
		$this->builder->where('helmet_image_id', $helmet_image_id);
		$this->builder->delete();
	}
}


?>