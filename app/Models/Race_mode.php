<?php  

namespace App\Models;

use CodeIgniter\Model;

class Race_mode extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('drag_race_mode');
    }
	
	public function crud_read()
	{	
		//$this->builder->orderBy('race_mode_id', 'ASC');
		return $this->builder->get()->getResultArray();
	}
	
}


?>