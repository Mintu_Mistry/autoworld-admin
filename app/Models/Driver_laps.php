<?php  

namespace App\Models;

use CodeIgniter\Model;

class Driver_laps extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('driver_laps');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($driver_id = '', $laps_id  = '')
	{	
		if($driver_id){
			$this->builder->where('driverid', $driver_id);
			return $this->builder->get()->getResultArray();
		}
		elseif ($laps_id) {
			$this->builder->where('laps_id', $laps_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('laps_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $laps_id)
	{	
		$this->builder->where("laps_id",$laps_id);
		$this->builder->update($data);
	}

	public function crud_delete($laps_id)
	{	
		$this->builder->where('laps_id', $laps_id);
		$this->builder->delete();
	}

	public function driver_details_delete($driver_id)
	{	
		$this->builder->where('driverid', $driver_id);
		$this->builder->delete();
	}

	public function lap_exist($driver_id, $laps)
	{
		$this->builder->where('driverid', $driver_id)->where('no_laps', $laps);
		return $this->builder->get()->getResultArray();
	}
}


?>