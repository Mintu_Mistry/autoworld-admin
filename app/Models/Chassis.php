<?php  

namespace App\Models;

use CodeIgniter\Model;

class Chassis extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('chassis_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($chassis_id = '')
	{	
		if($chassis_id){
			$this->builder->where('chassis_id', $chassis_id);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('chassis_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $chassis_id )
	{	
		$this->builder->where("chassis_id",$chassis_id );
		$this->builder->update($data);
	}

	public function crud_delete($chassis_id)
	{	
		$this->builder->where('chassis_id', $chassis_id);
		$this->builder->delete();
	}
}


?>