<?php  

namespace App\Models;

use CodeIgniter\Model;

class User_driver extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users_driver');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($user_id = '', $driver_id = '')
	{	
		if($user_id){
			$this->builder->where('userid', $user_id);
			return $this->builder->get()->getResultArray();
		}
		elseif ($driver_id) {
			$this->builder->where('driver_id', $driver_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('driver_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $driver_id)
	{	
		$this->builder->where("driver_id",$driver_id);
		$this->builder->update($data);
	}

	public function crud_delete($driver_id)
	{	
		$this->builder->where('driver_id', $driver_id);
		$this->builder->delete();
	}

	public function user_details_delete($userid)
	{	
		$this->builder->where('userid', $userid);
		$this->builder->delete();
	}
}


?>