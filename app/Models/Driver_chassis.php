<?php  

namespace App\Models;

use CodeIgniter\Model;

class Driver_chassis extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('driver_chassis');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($driver_id = '',$chassisid = '')
	{	
		if($driver_id){
			
			if($chassisid){
				
				$this->builder->where('driver_id', $driver_id);
				$this->builder->where('chassisid', $chassisid);
				$this->builder->orderBy('driver_chassis_id', 'DESC');
				return $this->builder->get()->getResultArray();
			}else{
				$this->builder->where('driver_id', $driver_id);
				$this->builder->orderBy('driver_chassis_id', 'DESC');
				return $this->builder->get()->getResultArray();
			}
		}
		else {
			$this->builder->orderBy('driver_chassis_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $driver_chassis_id)
	{	
		$this->builder->where('driver_chassis_id', $driver_chassis_id);
		$this->builder->delete();
	}

	public function crud_delete($driver_chassis_id)
	{	
		$this->builder->where('driver_chassis_id', $driver_chassis_id);
		$this->builder->delete();
	}
}


?>