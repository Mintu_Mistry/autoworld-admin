<?php  

namespace App\Models;

use CodeIgniter\Model;

class Race_type extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('race_types');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($race_type_id  = '')
	{	
		if ($race_type_id) {
			$this->builder->where('race_type_id', $race_type_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('race_type_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function read_driver_race_type($driver_id, $race_type)
	{
		$this->builder->where('driverid', $driver_id)->where('race_type', $race_type);
		return $this->builder->get()->getResultArray();
	}
}


?>