<?php  

namespace App\Models;

use CodeIgniter\Model;

class Driver_lane extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('ready_to_race');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($driver_id = '')
	{	
		//race_id	driver_id	lane_type	created_at
		if($driver_id){
			$this->builder->where('driver_id', $driver_id);
			$this->builder->orderBy('race_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('race_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}


	public function crud_update($data, $race_id)
	{	
		$this->builder->where('race_id', $race_id);
		$this->builder->update($data);
	}

	public function crud_delete($race_id)
	{	
		$this->builder->where('race_id', $race_id);
		$this->builder->delete();
	}
}


?>