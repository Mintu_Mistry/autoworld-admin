<?php  

namespace App\Models;

use CodeIgniter\Model;

class Driver_course extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('driver_course');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($driver_id = '', $course_id = '')
	{	
		if($driver_id){
			$this->builder->where('driverid', $driver_id);
			return $this->builder->get()->getResultArray();
		}
		elseif ($course_id) {
			$this->builder->where('course_id', $course_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('course_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $course_id)
	{	
		$this->builder->where("course_id",$course_id);
		$this->builder->update($data);
	}

	public function crud_delete($course_id)
	{	
		$this->builder->where('course_id', $course_id);
		$this->builder->delete();
	}

	public function driver_details_delete($driver_id)
	{	
		$this->builder->where('driverid', $driver_id);
		$this->builder->delete();
	}

	public function course_exist($driver_id,$course_id)
	{
		$this->builder->where('driverid', $driver_id)->where('course_id', $course_id);
		return $this->builder->get()->getResultArray();
	}
}


?>