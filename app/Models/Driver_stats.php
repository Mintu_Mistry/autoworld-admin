<?php  

namespace App\Models;

use CodeIgniter\Model;

class Driver_stats extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('driver_stats');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($driver_id = '', $stats_id = '')
	{	
		if($driver_id){
			$this->builder->where('driverid', $driver_id);
			return $this->builder->get()->getResultArray();
		}
		elseif ($stats_id) {
			$this->builder->where('stats_id', $stats_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('stats_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $stats_id)
	{	
		$this->builder->where("stats_id",$stats_id);
		$this->builder->update($data);
	}

	public function crud_delete($stats_id)
	{	
		$this->builder->where('stats_id', $stats_id);
		$this->builder->delete();
	}

	public function driver_details_delete($driver_id)
	{	
		$this->builder->where('driverid', $driver_id);
		$this->builder->delete();
	}

	public function read_driver_stat($driver_id, $stat_type)
	{	
		$this->builder->where('driverid', $driver_id)->where('stat_type', $stat_type);
		return $this->builder->get()->getResultArray();
	}

	public function read_driver_speed($driver_id, $stat_type)
	{
		$this->builder->selectMax('speed');
		$this->builder->where('driverid', $driver_id)->where('stat_type', $stat_type);
		return $this->builder->get()->getResultArray();
	}

	public function read_driver_time($driver_id, $stat_type)
	{
		$this->builder->selectMin('completion_time');
		$this->builder->where('driverid', $driver_id)->where('stat_type', $stat_type);
		return $this->builder->get()->getResultArray();
	}
}


?>