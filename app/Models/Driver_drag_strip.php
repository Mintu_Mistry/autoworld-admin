<?php  

namespace App\Models;

use CodeIgniter\Model;

class Driver_drag_strip extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('driver_drag_strips');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($driver_id = '', $drag_strips_id = '')
	{	
		if($driver_id){
			$this->builder->where('driverid', $driver_id);
			return $this->builder->get()->getResultArray();
		}
		elseif ($drag_strips_id) {
			$this->builder->where('drag_strips_id', $drag_strips_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('drag_strips_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $drag_strips_id)
	{	
		$this->builder->where("drag_strips_id",$drag_strips_id);
		$this->builder->update($data);
	}

	public function crud_delete($drag_strips_id)
	{	
		$this->builder->where('drag_strips_id', $drag_strips_id);
		$this->builder->delete();
	}

	public function driver_details_delete($driver_id)
	{	
		$this->builder->where('driverid', $driver_id);
		$this->builder->delete();
	}

	public function drap_strip_exist($driver_id, $track_name, $length)
	{
		$this->builder->where('driverid', $driver_id)->where('track_name', $track_name)->where('length', $length);
		return $this->builder->get()->getResultArray();
	}
}


?>