<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/helmet_images_management">Helmet Images Management</a>
				</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Helmet Images List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_image">Add Helmet Image </div>
			</div>
			<!----------------  Modal for Add Images ----------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_image" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Helmet Image
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
			                </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addHelmetImage" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Helmet Image *</label>

											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="helmet_image" required="" />
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
<!---------------------------- helmet images List ---------------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Helmet Image</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($helmet_image_details as $helmet_image){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><img src="<?= base_url()."/writable/uploads/".$helmet_image['image_path']?>" height="100px" width="100px" alt="Image"></td>
								<td>
									
									<a href="<?php base_url(); ?>deleteHelmetImage/<?=$helmet_image['helmet_image_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
