<?php
use App\Models\User_driver;
$user_driver = new User_driver();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/users_management">User Management</a>
				</li>
			</ul>
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Users List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#add_user">Add User </div>
			</div>
			<!----------------  Modal for User ----------------------->
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="add_user" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add User
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
		                  	</h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('/admin/addUser'); ?>" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="full_name" required="" placeholder="Full Name" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email *</label>
											<div class="col-sm-9">
												<input type="email" id="form-field-1" class="col-xs-10 col-sm-5" name="email" required="" placeholder="Email" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Mobile *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="phone_number" required="" onkeypress="return isNumber(event)" minlength="10" maxlength="10" placeholder="User Mobile" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="password" required="" minlength="8" placeholder="Password" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Track Lane *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="track_lane" required="" placeholder="Track Lane" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Track length *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="track_length" required="" onkeypress="return isNumber(event)" placeholder="Track length" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Profile Image *</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="profile_image" required="" accept="image/*"/>
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!----------------  Modal for edit user ----------------------->
			<?php
				foreach($users_details as $user){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_user<?=$user['user_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit User
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      <span aria-hidden="true">×</span>
			                    </button>
			                </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editUser/<?=$user['user_id']?>" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Full Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="full_name" required="" placeholder="Full Name" value="<?= $user['full_name']?>" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email *</label>
											<div class="col-sm-9">
												<input type="email" id="form-field-1" class="col-xs-10 col-sm-5" name="email" required="" placeholder="Email" value="<?= $user['email']?>" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Mobile *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="phone_number" required="" onkeypress="return isNumber(event)" minlength="10" maxlength="10" placeholder="User Mobile" value="<?= $user['phone_number']?>"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="password" required="" minlength="8" placeholder="Password" value="<?= $user['password']?>"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Track Lane *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="track_lane" required="" placeholder="Track Lane"  value="<?= $user['track_lane']?>"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Track length *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" class="col-xs-10 col-sm-5" name="track_length" required="" onkeypress="return isNumber(event)" placeholder="Track length"  value="<?= $user['track_length']?>"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Profile Image *</label>
											<div class="col-sm-9">
												<input type="file" id="form-field-1" class="col-xs-10 col-sm-8" name="profile_image" accept="image/*"/>
											</div>
										</div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
		<!------------------- Users List --------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Full Name</th>
								<th scope="col">Email</th>
								<th scope="col">Mobile</th>
								<th scope="col">Password</th>
								<th scope="col">Profile Image</th>
								<th scope="col">Track Lane</th>
								<th scope="col">Track Length</th>
								<th scope="col">Block Status</th>
								<th scope="col">User Driver</th>
								<!-- <th scope="col">User Stats</th>
								<th scope="col">User Laps</th> -->
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($users_details as $user){ 
								$snum += 1;
								$driver_data = count($user_driver->crud_read($user['user_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $user['full_name']?></td>
								<td><?= $user['email']?></td>
								<td><?= $user['phone_number']?></td>
								<td><?= $user['password']?></td>
								<td>
									<img src="<?= base_url()."/writable/uploads/".$user['profile_image']?>" height="100px" width="100px" alt="Profile Image">
								</td>
								<td><?= $user['track_lane']?></td>
								<td><?= $user['track_length']?></td>
								<td style="width: 75px;">
									<?php if($user['block_status'] == 1){?>
										<span class="label label-sm label-success" ><a href="<?=base_url('admin')?>/blockUser/<?=$user['user_id']?>" style="color:#FFF;padding-left: 21px; padding-right: 21px;">Unblock
										</a></span>
									<?php } else{ ?>
									<span class="label label-sm label-danger" ><a href="<?=base_url('admin')?>/blockUser/<?=$user['user_id']?>" style="color:#FFF;padding-left: 21px; padding-right: 21px;">Block</span>
										</a>
									<?php } ?>
								</td>
								<td>
									<?php 
										if($driver_data > 0){ ?>
											<a href="<?php base_url(); ?>users_driver_management/<?=$user['user_id']?>">
												<span style="margin-left: 40px;"><?php echo $driver_data;?></span>
											</a>
									<?php }
										else {?>
											<span style="margin-left: 40px;"><?php echo $driver_data;?></span>
										<?php }
									?>
								</td>
								<td>
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_user<?=$user['user_id']?>"></i>
									</span>
									<a href="<?php base_url(); ?>deleteUser/<?=$user['user_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function isNumber(evt) {
	    evt = (evt) ? evt : window.event;
	    var charCode = (evt.which) ? evt.which : evt.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	        alert("Enter numbers only");
	        return false;
	    }
	    return true;
	}
</script>