<ul class="nav nav-list">
	<li class="active">
		<a href="<?php echo base_url(); ?>">
			<i class="menu-icon fa fa-tachometer"></i>
			<span class="menu-text"> Dashboard </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/users_management">
			<i class="menu-icon fa fa-users"></i>
			<span class="menu-text"> Users Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/content_management">
			<i class="menu-icon glyphicon glyphicon-align-left"></i>
			<span class="menu-text"> Content Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/contact_management">
			<i class="menu-icon glyphicon glyphicon-envelope"></i>
			<span class="menu-text"> Contact Us Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/helmet_images_management">
			<i class="menu-icon glyphicon glyphicon-picture"></i>
			<span class="menu-text"> Helmet Image Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/chassis_management">
			<i class="menu-icon glyphicon glyphicon-road"></i>
			<span class="menu-text"> Chassis Management </span>
		</a>
		<b class="arrow"></b>
	</li>

</ul>