<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/driver_drag_strip_management/<?=$driver_id;?>">Driver Drag Strip Management</a>
				</li>
			</ul><!-- /.breadcrumb -->

		</div>

		<div class="page-content">
			<div class="page-header">
				<h1>
					Driver Drag Strip List
				</h1>
			</div>
<!---------------------------- Driver Drag Strip List ---------------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Track Name</th>
								<th scope="col">Length</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($driver_strip_details as $driver_strip){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $driver_strip['track_name']?></td>
								<td><?= $driver_strip['length']?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
