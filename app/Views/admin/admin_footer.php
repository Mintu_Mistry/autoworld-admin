<div class="footer">
	<div class="footer-inner">
		<div class="footer-content">
			<span class="bigger-120">
				<span class="blue bolder">Auto World</span>
				Application &copy; 2020-2021
			</span>

			&nbsp; &nbsp;
			
		</div>
	</div>
</div>

	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>
</div>

<script type="text/javascript">
if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>/public/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url(); ?>/public/assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.sparkline.index.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.flot.resize.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/ace-elements.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/ace.min.js"></script>

<!-- toastr script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
	$(function() {
		
		toastr.options.preventDuplicates = true;
		toastr.options.closeButton = true;
		toastr.options.positionClass = "toast-top-center";
		<?php $this->session = \Config\Services::session();
			if($this->session->success_msg){ ?>
				toastr.success("<?php echo $this->session->success_msg; ?>");
		<?php } if($this->session->error_msg){?>
				toastr.error("<?php echo $this->session->error_msg; ?>");
		<?php } ?>

		$('.fa-trash-o').click(function(event){
			if (confirm("Are you sure you want to delete?")) {
				return true;
			}
			else {
				event.preventDefault();
			}
		});

		/*--Data Table --*/
		var myTable = 
		$('#dynamic-table')
			.DataTable({
				"oSearch": { "bSmart": false, "bRegex": true }
		});
	});
</script>
</body>

</html>