

<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/view_driver_details/<?=$driver_id;?>">Driver Result Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Driver Results
				</h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Top Road Course Speed </label>

						<div class="col-sm-9">
							<?php
								if($course_speed){
							?>
								<span><?= $course_speed[0]['speed'];?></span>
							<?php } else { ?>
								<span></span>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Top Drag Strip Speed </label>

						<div class="col-sm-9">
							<?php
								if($course_speed){
							?>
								<span><?= $strip_speed[0]['speed'];?></span>
							<?php } else { ?>
								<span></span>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Best Road Course Time </label>

						<div class="col-sm-9">
							<?php
								if($course_time){
							?>
								<span><?= $course_time[0]['completion_time'];?></span>
							<?php } else { ?>
								<span></span>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Best Drag Strip Time </label>

						<div class="col-sm-9">
							<?php
								if($strip_time){
							?>
								<span><?= $strip_time[0]['completion_time'];?></span>
							<?php } else { ?>
								<span></span>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Race as a Pro </label>

						<div class="col-sm-9">
							<span><?= $race_pro_details;?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Race as a Sports Man </label>

						<div class="col-sm-9">
							<span><?= $race_sport_details;?></span>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
</div>
