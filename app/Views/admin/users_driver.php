<?php
use App\Models\Helmet_images;
$helmet_images = new Helmet_images();

use App\Models\Chassis;
$chassis = new Chassis;

use App\Models\Driver_course;
$driver_course = new Driver_course();

use App\Models\Driver_drag_strip;
$driver_drag_strip = new Driver_drag_strip();

use App\Models\Driver_laps;
$driver_laps = new Driver_laps();

use App\Models\Driver_stats;
$driver_stats = new Driver_stats();

?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/users_driver_management/<?=$user_id;?>">User Driver Management</a>
				</li>
			</ul><!-- /.breadcrumb -->

		</div>

		<div class="page-content">
			<div class="page-header">
				<h1>
					User Driver List
				</h1>
			</div>
<!---------------------------- User Driver List ---------------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Driver Name</th>
								<th scope="col">Helmet Image</th>
								<th scope="col">Chassis Type</th>
								<th scope="col">Driver Road Course</th>
								<th scope="col">Driver Drag Strip</th>
								<th scope="col">Driver Laps</th>
								<th scope="col">Driver Stats</th>
								<th scope="col">Driver Results</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($users_driver_details as $users_driver){ 
								$snum += 1;
								$helmet_data = $helmet_images->crud_read($users_driver['helmet_imageid']);
								$chassis_type_data = $chassis->crud_read($users_driver['chassisid']);
								$course_data = count($driver_course->crud_read($users_driver['driver_id']));
								$drag_strip_data = count($driver_drag_strip->crud_read($users_driver['driver_id']));	
								$laps_data = count($driver_laps->crud_read($users_driver['driver_id']));
								$stats_data = count($driver_stats->crud_read($users_driver['driver_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $users_driver['driver_name']?></td>
								<td>
									<img src="<?= base_url()."/writable/uploads/".$helmet_data['image_path']?>" height="100px" width="100px" alt="Image">
								</td>
								<td>
									<?php 
										if($chassis_type_data){
									?>
										<span><?= $chassis_type_data[0]['chassis_type']?></span>
									<?php } else {?>
										<span></span>
									<?php } ?>
								</td>
								<td>
									<?php 
										if($course_data > 0){ ?>
											<a href="<?php echo base_url(); ?>/admin/driver_roadcourse_management/<?=$users_driver['driver_id']?>">
												<span style="margin-left: 40px;"><?php echo $course_data;?></span>
											</a>
									<?php }
										else {?>
											<span style="margin-left: 40px;"><?php echo $course_data;?></span>
										<?php }
									?>
								</td>
								<td>
									<?php 
										if($drag_strip_data > 0){ ?>
											<a href="<?php echo base_url(); ?>/admin/driver_drag_strip_management/<?=$users_driver['driver_id']?>">
												<span style="margin-left: 40px;"><?php echo $drag_strip_data;?></span>
											</a>
									<?php }
										else {?>
											<span style="margin-left: 40px;"><?php echo $drag_strip_data;?></span>
										<?php }
									?>
								</td>
								<td>
									<?php 
										if($laps_data > 0){ ?>
											<a href="<?php echo base_url(); ?>/admin/driver_laps_management/<?=$users_driver['driver_id']?>">
												<span style="margin-left: 40px;"><?php echo $laps_data;?></span>
											</a>
									<?php }
										else {?>
											<span style="margin-left: 40px;"><?php echo $laps_data;?></span>
										<?php }
									?>
								</td>
								<td>
									<?php 
										if($stats_data > 0){ ?>
											<a href="<?php echo base_url(); ?>/admin/driver_stats_management/<?=$users_driver['driver_id']?>">
												<span style="margin-left: 40px;"><?php echo $stats_data;?></span>
											</a>
									<?php }
										else {?>
											<span style="margin-left: 40px;"><?php echo $stats_data;?></span>
										<?php }
									?>
								</td>
								<td>
									<a href="<?php echo base_url(); ?>/admin/view_driver_details/<?=$users_driver['driver_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="blue" style="margin-left: 40px;">
											View
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
