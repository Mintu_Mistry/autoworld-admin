
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/contact_management">Contact Us Management</a>
				</li>
			</ul><!-- /.breadcrumb -->

		</div>
		
		<div class="page-content">
			<div class="page-header">
				<h1>
					Contact Us List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" id="query_export">Export </div>
			</div>
			<script type="text/javascript">
			  	$("#query_export").click(function(){
			  		location.href = "<?=base_url('admin')?>/export_contact_csv";
			  	});
		  	</script>
<!---------------------------- Contact Us List ---------------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Name</th>
								<th scope="col">Email</th>
								<th scope="col">Message</th>
								<th scope="col">Date</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($contact_query as $contacts){ 

								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $contacts['user_name']?></td>
								<td><?= $contacts['email']?></td>
								<td><?= $contacts['message']?></td>
								<td><?= date('m-d-Y', strtotime($contacts['created_at']))?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
