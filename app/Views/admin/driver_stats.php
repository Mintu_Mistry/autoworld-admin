<?php
use App\Models\Driver_course;
$driver_course = new Driver_course();

use App\Models\Driver_drag_strip;
$driver_drag_strip = new Driver_drag_strip();

use App\Models\Chassis;
$chassis = new Chassis();
?>

<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?= base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/driver_stats_management/<?=$driver_id;?>">Driver Stats Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Driver Stats List
				</h1>
			</div>
		<!-------------------- Driver Stats List --------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Driver Name</th>
								<th scope="col">Stat Type</th>
								<th scope="col">Strip Length</th>
								<th scope="col">Course Length</th>
								<th scope="col">Chassis Type</th>
								<th scope="col">Time</th>
								<th scope="col">Date</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($driver_stats_details as $driver_stats){ 
								$snum += 1;
								
								if(!empty($driver_stats['drag_stripid'])){
									$strip_data = $driver_drag_strip->crud_read('', $driver_stats['drag_stripid']);
								}
								else {
									$strip_data = "";
								}
								if(!empty($driver_stats['courseid'])){
									$course_data = $driver_course->crud_read('', $driver_stats['courseid']);
								}
								else {
									$course_data = "";
								}
								$chassis_data = $chassis->crud_read($driver_stats['chassisid']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $driver_stats['user_name']?></td>
								<td>
									<?php
										if($driver_stats['stat_type'] == 1){?>
											<span>Road Course</span>
									<?php } else {?>
											<span>Drag Strip</span>
									<?php }?>
								</td>
								<td>
									<?php
										if($strip_data){ ?>
											<span><?= $strip_data[0]['length']?></span>
									<?php } else { ?>
										<span><?php echo ""; ?></span>
									<?php } ?>
								</td>
								<td>
									<?php
										if($course_data){ ?>
											<span><?= $course_data[0]['length']?></span>
									<?php } else { ?>
										<span><?php echo ""; ?></span>
									<?php } ?>
								</td>
								<td>
									<?php
										if($chassis_data){ ?>
											<span><?= $chassis_data[0]['chassis_type']?></span>
									<?php } else { ?>
										<span><?php echo ""; ?></span>
									<?php } ?>
								</td>
								<td><?= $driver_stats['completion_time']?></td>
								<td><?= date('m-d-Y', strtotime($driver_stats['created_at']))?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
