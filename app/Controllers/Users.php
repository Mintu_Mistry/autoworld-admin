<?php  

namespace App\Models;

use CodeIgniter\Model;

class Users extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users_details');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($user_id = '')
	{	
		if($user_id){
			$this->builder->where('user_id', $user_id);
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('user_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $user_id)
	{	
		$this->builder->where("user_id",$user_id);
		$this->builder->update($data);
	}

	public function crud_delete($user_id)
	{	
		$this->builder->where('user_id', $user_id);
		$this->builder->delete();
	}

	public function read_by_email($email)
	{	
		$this->builder->where('email', $email);
		return $this->builder->get()->getRowArray();
	}

	public function update_by_email($data, $email)
	{	
		$this->builder->where('email', $email);
		$this->builder->update($data);
	}

	public function read_by_mail_or_phone($mail = '', $mobile = '')
	{
		if($mail)
			return $this->builder->where("email", $mail)->get()->getResultArray();
		else 
			return $this->builder->where("phone_number", $mobile)->get()->getResultArray();
	}
	
	public function login($email='',$phone_number='',$password)
	{
		if($email!='')
		{
			$row = $this->builder->where("email",$email)->where("password", $password)->get()->getRow();
		}else if($phone_number!=''){
			$row = $this->builder->where("phone_number",$phone_number)->where("password", $password)->get()->getRow();
		}
		
		if (isset($row->user_id)) {
			return $row->user_id;
		}
		else{
			return 0;
		}
	}
	
}


?>