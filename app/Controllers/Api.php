<?php 
/*

	Auto World API controller

*/

namespace App\Controllers;

use Twilio\Rest\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;

use App\Models\Login;
use App\Models\Content;
use App\Models\Chassis;
use App\Models\Helmet_images;
use App\Models\Contact;
use App\Models\Users;
use App\Models\User_driver;
use App\Models\Driver_course;
use App\Models\Driver_laps;
use App\Models\Driver_drag_strip;
use App\Models\Driver_stats;
use App\Models\Driver_chassis;
use App\Models\Driver_lane;
use App\Models\Race_mode;


class Api extends BaseController

{
	//protected $upload;
	use ResponseTrait;
	private function apiResponse($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	private function generateOTP()
	{
		$otp = "";
      	$generator = "1357902468";
      	for ($i = 1; $i <= 4; $i++) { 
	        $otp .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	    return $otp;
	}

	public function index()
	{
		$returndata = array(
			"message" => "Auto World API"
		);
		return $this->apiResponse($returndata);
	}

	private function sendTwilioSMS($text,$to)
	{
		try{
			$twilio = new Client($this->TWILIO_SID, $this->TWILIO_TOKEN);
	        return $twilio->messages->create(
			    $to,
			    [
			        'from' => '+12673949525',
			        'body' => $text
			    ]
			);
		}
		catch(Exception $e){
			return 0;
		}
		finally {
			return 0;
		}
	}

	private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@auto_world.yesitlabs.xyz"));
	}

	public function get_content()
	{
		$returndata = array();
		$content = new Content();
		$content_details = $content->crud_read();
		
		if($content_details){
			$returndata['status'] = TRUE;
			$returndata['data'] = $content_details;
			$returndata['message'] = "Content fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_chassis_type()
	{
		$returndata = array();
		$chassis = new Chassis();
		$chassis_details = $chassis->crud_read();
		
		if($chassis_details){
			$returndata['status'] = TRUE;
			$returndata['data'] = $chassis_details;
			$returndata['message'] = "Chassis Type fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_helmet_images()
	{
		$returndata = array();
		$helmet_images = new Helmet_images();
		$helmet_details = $helmet_images->crud_read();
		
		if($helmet_details){
			$images = array();
			foreach ($helmet_details as $helmet) {
				$images[] = array(
					'helmet_image_id' => $helmet['helmet_image_id'],
					'image_path' => base_url()."/writable/uploads/".$helmet['image_path'],
					'createdat' => date('m-d-Y', strtotime($helmet['createdat']))
				);
			}
			$returndata['status'] = TRUE;
			$returndata['data'] = $images;
			$returndata['message'] = "Helmet Images fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function submit_contact_query()
	{
		$returndata = array();
		$contact = new Contact();
		$user_name = $this->request->getPost('user_name');
		$email = $this->request->getPost('email');
		$message = $this->request->getPost('message');
		if(!empty($user_name) && !empty($email) && !empty($message)){
			$contact->crud_create(array(
				'user_name' => $user_name,
				'email' => $email,
				'message' => $message,
				'created_at' => date('Y-m-d H:i:s')
			));
			$returndata['status'] = TRUE;
			$returndata['message'] = "Contact query submitted successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function check_email()
	{
		$returndata = array();
		$users = new Users();
		$email = $this->request->getPost("email");
		if(!empty($email)){
			$user_details = $users->read_by_email($email);
			if(!empty($user_details)){
				$returndata['status'] = true;
        		$returndata['message'] = "Email already registered!";
			}
			else{
				$returndata['status'] = true;
        		$returndata['message'] = "Email not registered!";
			}
		}
		else {
			$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}

	public function change_password()
    {
    	$returndata = array();
    	$users = new Users();
    	$user_id = $this->request->getPost("user_id");
    	$current_pass = $this->request->getPost('current_pass');
    	$password = $this->request->getPost('password');
    	if(!empty($user_id) && !empty($current_pass) && !empty($password) ){
			$user_data = $users->crud_read($user_id);
			if(count($user_data)>0)
			{
				if($current_pass == $user_data[0]['password'])
				{
					$users->crud_update(array(
					'password'=> $password
					), $user_id);
					
					$returndata['status'] = true;
					$returndata['message'] = "Password updated successfully.";
					
				}else{
					$returndata['status'] = false;
					$returndata['message'] = "Current Password not matched.";
				}
				
				
			}else {
				$returndata['status'] = false;
				$returndata['message'] = "User not exists.";
			}
    	}
    	else {
    		$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
    	}
    	$this->apiResponse($returndata);
    }
	
	public function recover_password()
    {
    	$returndata = array();
    	$users = new Users();
    	$email = $this->request->getPost('email');
    	$password = $this->request->getPost('password');
    	if(!empty($email) && !empty($password) ){
    		$users->update_by_email(array(
    			'password'=> $password
    		), $email);
			$returndata['status'] = true;
			$returndata['message'] = "Password updated successfully.";
    	}
    	else {
    		$returndata['status'] = false;
        	$returndata['message'] = "Please provide required fields.";
    	}
    	$this->apiResponse($returndata);
    }
	
    public function forget_password() 
    {
    	$returndata = array();
        $email = $this->request->getPost("email");
        $mobile = $this->request->getPost("phone");
        $users = new Users();
        if(!empty($email) || !empty($mobile)){
            $data = $users->read_by_mail_or_phone($email, $mobile);
            //echo $users->getLastQuery();
            if(!empty($data)){   
                $otp = $this->generateOTP();
                $message = 'Your Verification OTP Is :-'.$otp;
                if(!empty($email)){
                	$send_status = mail($email, "Forget Password", $message, array("from" => "no-reply@auto_world.yesitlabs.xyz"));
                	$users->update_by_email(array(
		    			'otp_code'=> $otp
		    		), $email);
	                if ($send_status) {
	                    $returndata['status'] = true;  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
	                } else {                               
	                    $returndata['status'] = false;                    
	                    $returndata['message'] = 'Sorry, there is some internal issue, please try again';
	                }
                }
                elseif(!empty($mobile)){
                	$result = $this->sendTwilioSMS($message, $mobile);
                	//print_r($result);
                	$returndata['status'] = true;  
                    $returndata['otp'] = $otp;                  
                    $returndata['message'] = 'OTP is sent on provided mobile number!' ;
                }
            }
            else {
                $returndata['status'] = false; 
                $returndata['message'] = 'User does not exist.' ;
            }
        }
        else {
            $returndata['status'] = false; 
            $returndata['message'] = 'Please provide required fields.' ;
        }
        $this->apiResponse($returndata);
    }

    public function verify_otp()
    {
    	$returndata = array();
    	$users = new Users();
    	$email = $this->request->getPost("email");
        $otp = $this->request->getPost("otp");
        if(!empty($email) && !empty($otp) ){   
        	$user_details = $users->read_by_email($email);
        	if($user_details['otp_code'] == $otp){
        		$returndata['status'] = true; 
            	$returndata['message'] = 'Correct OTP.' ;
        	}
        	else {
        		$returndata['status'] = false; 
            	$returndata['message'] = 'Wrong OTP.' ;
        	}
        }
        else {
        	$returndata['status'] = false; 
            $returndata['message'] = 'Please provide required fields.' ;
        }
        $this->apiResponse($returndata);
    }

	public function sign_up()
	{
		$returndata = array();
		$users = new Users();
		$full_name = $this->request->getPost("full_name");
		$email = $this->request->getPost("email");
		$phone_number = $this->request->getPost("phone_number");
		$password = $this->request->getPost("password");
		/* $track_length = $this->request->getPost("track_length");
		$track_lane = $this->request->getPost("track_lane");
		$profile_image = $this->request->getFile("profile_image"); */
		if(!empty($full_name) && !empty($email) && !empty($phone_number) && !empty($password)){
			/* if($profile_image){
				if($profile_image->isValid()){
					$profile_imagepath = $profile_image->store();
					if($profile_imagepath != ""){ */
					/* echo "sdfcs";
					die(); */
					$data = $users->read_by_mail_or_phone($email, $phone_number);
					if(empty($data))
					{
						$users->crud_create(
							array(
								"full_name" => $full_name,
								"email" => $email,
								"phone_number" => $phone_number,
								"password" => $password,
								//"track_lane" => $track_lane,
								//"track_length" => $track_length,
								//"profile_image" => $profile_imagepath,
								"created_at" => date('Y-m-d H:i:s')
							)
						);
						$returndata['status'] = TRUE;
						$returndata['message'] = "User signed up successfully.";
			
					}else{
						$returndata['status'] = FALSE;
						$returndata['message'] = "Email/Phone is already registered.";
					}
					/* }
				}
			} */
			
			
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function login()
	{
		$returndata = array();
		$users = new Users();
		$email = $this->request->getPost("email");
		$phone_number = $this->request->getPost("phone_number");
		$password = $this->request->getPost("password");
		if(!empty($password) && (!empty($email) || !empty($phone_number))){
			$userdata = $users->login($email,$phone_number,$password);
			if($userdata ==0){
				$returndata['status'] = "error";
				$returndata['message'] = "Invalid credentials!";
				return $this->respond($returndata,200);
			}
			else{
				$returndata['status'] = TRUE;
				$returndata['message'] = "Login success!";
			}
		}else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata,200);
	}
	
	
	public function update_user_profile()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		$full_name = $this->request->getPost("full_name");
		$email = $this->request->getPost("email");
		$phone_number = $this->request->getPost("phone_number");
		$track_length = $this->request->getPost("track_length");
		$track_lane = $this->request->getPost("track_lane");
		
		if(!empty($user_id) && !empty($full_name) && !empty($email) && !empty($phone_number)){
			
			$profile_image = $this->request->getFile("profile_image");
			if($profile_image){
				if($profile_image->isValid()){
					$profile_imagepath = $profile_image->store();
					if($profile_imagepath != ""){
						$users->crud_update(
							array(
								"full_name" => $full_name,
								"email" => $email,
								"phone_number" => $phone_number,
								"password" => $password,
								"track_lane" => $track_lane,
								"track_length" => $track_length,
								"profile_image" => $profile_imagepath,
								"created_at" => date('Y-m-d H:i:s')
							), $user_id
						);
					}else{
						$users->crud_update(
							array(
								"full_name" => $full_name,
								"email" => $email,
								"phone_number" => $phone_number,
								"password" => $password,
								"track_lane" => $track_lane,
								"track_length" => $track_length,
								"created_at" => date('Y-m-d H:i:s')
							), $user_id
						);
					}
				}
			}
			
			$returndata['status'] = TRUE;
			$returndata['message'] = "User profile updated successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_details()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$user_details = $users->crud_read($user_id);
			if($user_details){
				$returndata['status'] = TRUE;
				$returndata['data'] = $user_details;
				$returndata['message'] = "User details fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_user_driver_details()
	{
		$returndata = array();
		$user_driver = new User_driver();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$user_driver_details = $user_driver->crud_read($user_id);
			if($user_driver_details){
				$returndata['status'] = TRUE;
				$returndata['data'] = $user_driver_details;
				$returndata['message'] = "User driver details fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function create_driver()
	{
		$returndata = array();
		$driver = new User_driver();
		$driver_name = $this->request->getPost("driver_name");
		$userid = $this->request->getPost("userid");
		$helmet_imageid = $this->request->getPost("helmet_imageid");
		//$chassisid = $this->request->getPost("chassisid");
		if(!empty($driver_name) && !empty($helmet_imageid) && !empty($userid)){
			$driver->crud_create(array(
				"driver_name" => $driver_name,
				"userid" => $userid,
				"helmet_imageid" => $helmet_imageid,
				//"chassisid" => $chassisid,
				'created_at' => date('Y-m-d H:i:s')
			));
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver created successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function edit_driver()
	{
		$returndata = array();
		$driver = new User_driver();
		$driver_name = $this->request->getPost("driver_name");
		$userid = $this->request->getPost("userid");
		$driver_id = $this->request->getPost('driver_id');
		$helmet_imageid = $this->request->getPost("helmet_imageid");
		//$chassisid = $this->request->getPost("chassisid");
		if(!empty($driver_name) && !empty($helmet_imageid) && !empty($userid) && !empty($driver_id)){
			$driver->crud_update(array(
				"driver_name" => $driver_name,
				"helmet_imageid" => $helmet_imageid
				//"chassisid" => $chassisid
			),$driver_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver details updated successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function delete_driver()
	{
		$returndata = array();
		$driver = new User_driver();
		$driver_id = $this->request->getPost('driver_id');
		if(!empty($driver_id)){
			$driver->crud_delete($driver_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver deleted successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	public function create_driver_chassis()
	{
		$returndata = array();
		$driver = new Driver_chassis();
		$driver_id = $this->request->getPost('driver_id');
		$chassisid = $this->request->getPost("chassisid");
		if(!empty($driver_id) && !empty($chassisid)){
			//driver_chassis_id	driver_id	chassisid
			$driver->crud_create(array(
				"driver_id" => $driver_id,
				"chassisid" => $chassisid,
				'created_at' => date('Y-m-d H:i:s')
			));
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver Chassis created successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function edit_driver_chassis()
	{
		$returndata = array();
		$driver = new Driver_chassis();
		$driver_id = $this->request->getPost('driver_id');
		$driver_chassis_id = $this->request->getPost('driver_chassis_id');
		$chassisid = $this->request->getPost("chassisid");
		if(!empty($driver_chassis_id) && !empty($chassisid) && !empty($driver_id)){
			//driver_chassis_id	driver_id	chassisid
			$driver->crud_update(array(
				"driver_id" => $driver_id,
				"chassisid" => $chassisid
			),$driver_chassis_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver Chassis updated successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function get_driver_chassis_details()
	{
		$returndata = array();
		$driver = new Driver_chassis();
		$chassis = new Chassis;
		$driver_id = $this->request->getPost('driver_id');
		$driver_details = $driver->crud_read($driver_id);
		if($driver_details){
			$chassis_type_data = $chassis->crud_read($driver_details[0]['chassisid']);
			$driver_data[] = array(
				'driver_id' => $driver_details[0]['driver_id'],
				'chassis_type' => $chassis_type_data[0]['chassis_type'],
				'created_at' => date('m-d-Y', strtotime($driver_details[0]['created_at']))
			);
			$returndata['status'] = TRUE;
			$returndata['data'] = $driver_data;
			$returndata['message'] = "Driver Chassis details fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function delete_driver_chassis()
	{
		$returndata = array();
		$driver = new User_driver();
		$driver_id = $this->request->getPost('driver_id');
		$driver_chassis_id = $this->request->getPost('driver_chassis_id');
		if(!empty($driver_id) && !empty($driver_chassis_id)){
			$driver->crud_delete($driver_chassis_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver chasis deleted successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function get_driver_details_by_id()
	{
		$returndata = array();
		$user_driver = new User_driver();
		$chassis = new Chassis;
		$driver = new Driver_chassis();
		$helmet_images = new Helmet_images;
		$driver_id = $this->request->getPost('driver_id');
		$driver_details = $user_driver->crud_read('',$driver_id);
		if($driver_details){
			$driver_chassis = $driver->crud_read($driver_id);
			if($driver_details){
				$chassis_type_data = $chassis->crud_read($driver_chassis[0]['chassisid']);
				$helmet_image_data = $helmet_images->crud_read($driver_details[0]['helmet_imageid']);
				$driver_data[] = array(
					'driver_id' => $driver_details[0]['driver_id'],
					'userid' => $driver_details[0]['userid'],
					'driver_name' => $driver_details[0]['driver_name'],
					'chassis_type' => $chassis_type_data[0]['chassis_type'],
					'helmet_image' =>  base_url()."/writable/uploads/".$helmet_image_data['image_path'],
					'created_at' => date('m-d-Y', strtotime($driver_details[0]['created_at']))
				);
				$returndata['status'] = TRUE;
				$returndata['data'] = $driver_data;
				$returndata['message'] = "Driver details fetched successfully.";
			}else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function create_driver_course()
	{
		$returndata = array();
		$driver_course = new Driver_course();
		$driver_id = $this->request->getPost('driver_id');
		$course_name = $this->request->getPost('course_name');
		$length = $this->request->getPost('length');
		if(!empty($driver_id) && !empty($course_name) && !empty($length)){
			$course_exists = $driver_course->course_exist($driver_id, $course_name, $length);
			if(empty($course_exists)){
				$driver_course->crud_create(array(
					'driverid' => $driver_id,
					'course_name' => $course_name,
					'length' => $length,
					'created_at' => date('Y-m-d H:i:s')
				));
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver course created successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Driver course already exist.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function edit_driver_course()
	{
		$returndata = array();
		$driver_course = new Driver_course();
		$course_id = $this->request->getPost('course_id');
		$driver_id = $this->request->getPost('driver_id');
		$course_name = $this->request->getPost('course_name');
		$length = $this->request->getPost('length');
		if(!empty($course_id) && !empty($driver_id) && !empty($course_name) && !empty($length)){
			$course_exists = $driver_course->course_exist($driver_id,$course_id);
			if(!empty($course_exists)){
				$driver_course->crud_update(array(
					'driverid' => $driver_id,
					'course_name' => $course_name,
					'length' => $length,
					'created_at' => date('Y-m-d H:i:s')
				), $course_id);
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver course updated successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Driver course not exist.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function delete_driver_course()
	{
		$returndata = array();
		$driver_course = new Driver_course();
		$course_id = $this->request->getPost('course_id');
		if(!empty($course_id)){
			$driver_course->crud_delete($course_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver course deleted successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_driver_road_course()
	{
		$returndata = array();
		$driver_course = new Driver_course();
		$driver_id = $this->request->getPost('driver_id');
		$course_details = $driver_course->crud_read($driver_id);
		if($course_details){
			$returndata['status'] = TRUE;
			$returndata['data'] = $course_details;
			$returndata['message'] = "Driver road course fetched successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "No data found.";
		}
		return $this->apiResponse($returndata);
	}

	public function create_driver_laps()
	{
		$returndata = array();
		$driver_laps = new Driver_laps();
		$driver_id = $this->request->getPost('driver_id');
		$laps = $this->request->getPost('laps');
		if(!empty($driver_id) && !empty($laps)){
			$laps_exist = $driver_laps->lap_exist($driver_id, $laps);
			if(empty($laps_exist)){
				$driver_laps->crud_create(array(
					'driverid' => $driver_id,
					'no_laps' => $laps,
					'created_at' => date('Y-m-d H:i:s')
				));
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver laps created successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Lap already exist.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_driver_laps()
	{
		$returndata = array();
		$driver_laps = new Driver_laps();
		$driver_id = $this->request->getPost('driver_id');
		if(!empty($driver_id)){
			$driver_laps_details = $driver_laps->crud_read($driver_id);
			if($driver_laps_details){
				$laps_array = array();
				foreach ($driver_laps_details as $laps) {
					$laps_array[] = array(
						'laps_id' => $laps['laps_id'],
						'driverid' => $laps['driverid'],
						'no_laps' => $laps['no_laps'],
						'createdat' => date('m-d-Y', strtotime($laps['created_at']))
					);
				}
				$returndata['status'] = TRUE;
				$returndata['data'] = $laps_array;
				$returndata['message'] = "Driver laps fetched successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function create_driver_drag_strips()
	{
		$returndata = array();
		$driver_drag_strip = new Driver_drag_strip();
		$driver_id = $this->request->getPost('driver_id');
		$track_name = $this->request->getPost('track_name');
		$length = $this->request->getPost('length');
		if(!empty($driver_id) && !empty($track_name) && !empty($length)){
			$drag_exist = $driver_drag_strip->drap_strip_exist($driver_id, $track_name, $length);
			if(empty($drag_exist)){
				$driver_drag_strip->crud_create(array(
					'driverid' => $driver_id,
					'track_name' => $track_name,
					'length' => $length,
					'created_at' => date('Y-m-d H:i:s')
				));
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver drag strip created successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Driver drag strip already exist.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function edit_driver_drag_strips()
	{
		$returndata = array();
		$driver_drag_strip = new Driver_drag_strip();
		$drag_strips_id = $this->request->getPost('drag_strips_id');
		$driver_id = $this->request->getPost('driver_id');
		$track_name = $this->request->getPost('track_name');
		$length = $this->request->getPost('length');
		if(!empty($drag_strips_id) && !empty($driver_id) && !empty($track_name) && !empty($length)){
			$drag_exist = $driver_drag_strip->drap_strip_exist($driver_id, $track_name, $length);
			if(empty($drag_exist)){
				$driver_drag_strip->crud_update(array(
					'driverid' => $driver_id,
					'track_name' => $track_name,
					'length' => $length,
					'created_at' => date('Y-m-d H:i:s')
				), $drag_strips_id);
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver drag strips updated successfully.";
			}
			else {
				$returndata['status'] = FALSE;
				$returndata['message'] = "Driver drag strip already exist.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function delete_driver_drag_strips()
	{
		$returndata = array();
		$driver_drag_strip = new Driver_drag_strip();
		$drag_strips_id = $this->request->getPost('drag_strips_id');
		if(!empty($drag_strips_id)){
			$driver_drag_strip->crud_delete($drag_strips_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver drag strips deleted successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_driver_drag_strips()
	{
		$returndata = array();
		$driver_drag_strip = new Driver_drag_strip();
		$driver_id = $this->request->getPost('driver_id');
		if(!empty($driver_id)){
			$driver_drag_strip_details = $driver_drag_strip->crud_read($driver_id);
			if($driver_drag_strip_details){
				$returndata['status'] = TRUE;
				$returndata['data'] = $driver_drag_strip_details;
				$returndata['message'] = "Driver drag strips fetched successfully.";
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function ready_to_race()
	{
		$returndata = array();
		$driver_lane = new Driver_lane();
		$driver_id = $this->request->getPost('driver_id');
		$lane_type = $this->request->getPost('lane_type');
		if(!empty($driver_id) && !empty($lane_type)){
			$lane_exist = $driver_lane->crud_read($driver_id);
			/* echo "<pre>";
			print_r($lane_exist);
			die(); */
			if(empty($lane_exist)){
				
				//race_id	driver_id	lane_type	created_at
				$driver_lane->crud_create(array(
					'driver_id' => $driver_id,
					'lane_type' => $lane_type,
					'created_at' => date('Y-m-d H:i:s')
				));
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver lane created successfully.";
			}else{
				
				$driver_lane->crud_update(array(
					'lane_type' => $lane_type
				),$lane_exist[0]['race_id']);
				
				$returndata['status'] = TRUE;
				$returndata['message'] = "Driver lane created successfully.";
				
			}
			
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function get_driver_race()
	{
		$returndata = array();
		$driver_lane = new Driver_lane();
		$user_driver = new User_driver();
		$driver_id = $this->request->getPost('driver_id');
		if(!empty($driver_id)){
			$driver_lane_details = $driver_lane->crud_read($driver_id);
			
			if($driver_lane_details){
				
				$lane_arr =array();
				foreach($driver_lane_details as $driver_lane)
				{
					$driver_details = $user_driver->crud_read('',$driver_id);
					if(count($driver_details)>0)
					{
						$driver_lane['driver_name'] = $driver_details[0]['driver_name'];
						$lane_arr[] = $driver_lane;
					}
					
				}
				if(empty($lane_arr))
				{
					$returndata['status'] = FALSE;
					$returndata['message'] = "No data found.";
				}else{
					$returndata['status'] = TRUE;
					$returndata['data'] = $lane_arr;
					$returndata['message'] = "Driver race fetched successfully.";
				}
			}
			else{
				$returndata['status'] = FALSE;
				$returndata['message'] = "No data found.";
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	public function create_driver_stats()
	{
		$returndata = array();
		$driver_stats = new Driver_stats();
		$stat_type = $this->request->getPost('stat_type');
		$driver_id = $this->request->getPost('driver_id');
		$user_name = $this->request->getPost('user_name');
		$drag_stripid = $this->request->getPost('drag_strip_id');
		$courseid = $this->request->getPost('course_id');
		$chassisid = $this->request->getPost('chassis_id');
		$completion_time = $this->request->getPost('completion_time');
		$speed = $this->request->getPost('speed');
		if(!empty($driver_id) && !empty($stat_type) && !empty($completion_time)){
			if($stat_type == 1){
				$driver_stats->crud_create(array(
					'stat_type' => 1,
					'driverid' => $driver_id,
					'user_name' => $user_name,
					'drag_stripid' => 0,
					'courseid' => $courseid,
					'chassisid' => $chassisid,
					'speed' => $speed,
					'completion_time' => $completion_time,
					'created_at' => date('Y-m-d H:i:s')
				));
			}
			else {
				$driver_stats->crud_create(array(
					'stat_type' => 2,
					'driverid' => $driver_id,
					'user_name' => $user_name,
					'drag_stripid' => $drag_stripid,
					'courseid' => 0,
					'chassisid' => $chassisid,
					'speed' => $speed,
					'completion_time' => $completion_time,
					'created_at' => date('Y-m-d H:i:s')
				));
			}
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver stat created successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function clear_driver_stats()
	{
		$returndata = array();
		$driver_stats = new Driver_stats();
		$driver_id = $this->request->getPost('driver_id');
		if(!empty($driver_id)){
			$driver_stats->driver_details_delete($driver_id);
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver stats cleared successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function get_driver_stats()
	{
		$returndata = array();
		$driver_stats = new Driver_stats();
		$driver_drag_strip = new Driver_drag_strip();
		$driver_course = new Driver_course();
		$chassis = new Chassis();
		$driver_id = $this->request->getPost('driver_id');
		$stat_type = $this->request->getPost('stat_type');
		if(!empty($driver_id) && !empty($stat_type)){
			if($stat_type == 1){
				$driver_stat_details = $driver_stats->read_driver_stat($driver_id, $stat_type);
				if($driver_stat_details){
					$driver_stat_array = array();
					foreach ($driver_stat_details as $driver_stat) {
						$course_details = $driver_course->crud_read('', $driver_stat['courseid']);
						$chassis_details = $chassis->crud_read($driver_stat['chassisid']);
						$driver_stat_array[] = array(
							'stats_id' => $driver_stat['stats_id'],
							'stat_type' => "Road Course",
							'user_name' => $driver_stat['user_name'],
							'course_name' => $course_details[0]['course_name'],
							'chassis_type' => $chassis_details[0]['chassis_type'],
							'completion_time' => $driver_stat['completion_time'],
							'created_at' => date('m-d-Y', strtotime($driver_stat['created_at']))
						);
					}
					$returndata['status'] = TRUE;
					$returndata['data'] = $driver_stat_array;
					$returndata['message'] = "Driver stats fetched successfully.";
				}
				else {
					$returndata['status'] = FALSE;
					$returndata['message'] = "No data found.";
				}
			}
			else {
				$driver_stat_details = $driver_stats->read_driver_stat($driver_id, $stat_type);
				if($driver_stat_details){
					$driver_stat_array = array();
					foreach ($driver_stat_details as $driver_stat) {
						$driver_drag_strip_details = $driver_drag_strip->crud_read('', $driver_stat['drag_stripid']);
						$chassis_details = $chassis->crud_read($driver_stat['chassisid']);
						$driver_stat_array[] = array(
							'stats_id' => $driver_stat['stats_id'],
							'stat_type' => "Drag Strip",
							'user_name' => $driver_stat['user_name'],
							'strip_length' => $driver_drag_strip_details[0]['length'],
							'chassis_type' => $chassis_details[0]['chassis_type'],
							'completion_time' => $driver_stat['completion_time'],
							'created_at' => date('m-d-Y', strtotime($driver_stat['created_at']))
						);
					}
					$returndata['status'] = TRUE;
					$returndata['data'] = $driver_stat_array;
					$returndata['message'] = "Driver stats fetched successfully.";
				}
				else {
					$returndata['status'] = FALSE;
					$returndata['message'] = "No data found.";
				}
			}
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}

	public function calculate_length()
	{
		$returndata = array();
		$straight15 = $this->request->getPost('straight15');
		$straight9 = $this->request->getPost('straight9');
		$straight6 = $this->request->getPost('straight6');
		$straight5 = $this->request->getPost('straight5');
		$straight3 = $this->request->getPost('straight3');

		$curv9 = $this->request->getPost('curv9');
		$curv12 = $this->request->getPost('curv12');
		$curv15 = $this->request->getPost('curv15');
		$curv6 = $this->request->getPost('curv6');
		$curv18 = $this->request->getPost('curv18');

		$jumpland9 = $this->request->getPost('jumpland9');
		$chicane9 = $this->request->getPost('chicane9');
		$hairpin9 = $this->request->getPost('hairpin9');

		$length_inch = (15*$straight15 + 9*$straight9 + 6*$straight6 + 5*$straight5 + 3*$straight3 + 9*$curv9 + 12*$curv12 + 15*$curv15 + 6*$curv6 + 18*$curv18 + 9*$jumpland9 + 9*$chicane9 + 9*$hairpin9);
		$length = ($length_inch/12);
		$returndata['status'] = TRUE;
		$returndata['length'] = $length;
		$returndata['message'] = "Length calculated successfully.";
		return $this->apiResponse($returndata);
	}
	
	public function create_multiple_driver_chassis()
	{
		$returndata = array();
		$driver = new Driver_chassis();
		$driver_chassis_arr = $this->request->getPost('driver_chassis_arr');
		if(!empty($driver_chassis_arr)){
	/* driver_chassis_arr = '{"chassisList": [{"driverId": "4","chassisId": "1"},{"driverId": "5","chassisId": "4"},{"driverId": "6","chassisId": "2"}]}'; */
				$res = json_decode($driver_chassis_arr);
				$response = $res->chassisList;
				foreach($response as $driver_chassis_data)
				{
					$old_record = $driver->crud_read($driver_chassis_data->driverId,$driver_chassis_data->chassisId);
	
					if(count($old_record)==0)
					{
						$driver->crud_create(array(
						"driver_id" => $driver_chassis_data->driverId,
						"chassisid" => $driver_chassis_data->chassisId,
						'created_at' => date('Y-m-d H:i:s')
						));
					}
					
				}
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver Chassis created successfully.";
		}
		else {
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function ready_to_race_multiple_driver()
	{
		$returndata = array();
		$driver_lane = new Driver_lane();
		$driver_lane_arr = $this->request->getPost('driver_lane_arr');
		if(!empty($driver_lane_arr)){
			
		/* driver_lane_arr = '{"LaneList": [{"driverId": "4","laneType": "1"},{"driverId": "5","laneType": "4"},{"driverId": "3","laneType": "2"}]}'; */
			$res = json_decode($driver_lane_arr);
			/* echo "<pre>";
			print_r($res);
			die(); */
			$response = $res->LaneList;
			foreach($response as $driver_lane_data)
			{	
				$lane_exist = $driver_lane->crud_read($driver_lane_data->driverId);
				
				if(empty($lane_exist)){
					
					//race_id	driver_id	lane_type	created_at
					$driver_lane->crud_create(array(
						'driver_id' => $driver_lane_data->driverId,
						'lane_type' => $driver_lane_data->laneType,
						'created_at' => date('Y-m-d H:i:s')
					));
					
				}else{
					
					$driver_lane->crud_update(array(
						'lane_type' => $driver_lane_data->laneType
					),$lane_exist[0]['race_id']);
					
					
				}
			}
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver lane created successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	
	public function create_multiple_driver_laps()
	{
		$returndata = array();
		$driver_laps = new Driver_laps();
		$driver_laps_arr = $this->request->getPost('driver_laps_arr');
		if(!empty($driver_laps_arr)){
			
		/* driver_laps_arr = '{"LapsList": [{"driverId": "4","laps": "1"},{"driverId": "5","laps": "4"},{"driverId": "3","laps": "2"}]}'; */
			$res = json_decode($driver_laps_arr);
			/* echo "<pre>";
			print_r($res);
			die(); */
			$response = $res->LapsList;
			foreach($response as $driver_laps_data)
			{	
				$laps_exist = $driver_laps->lap_exist($driver_laps_data->driverId, $driver_laps_data->laps);
				if(empty($laps_exist)){
					$driver_laps->crud_create(array(
						'driverid' => $driver_laps_data->driverId,
						'no_laps' => $driver_laps_data->laps,
						'created_at' => date('Y-m-d H:i:s')
					));
					
				}
				else {
					$driver_laps->crud_update(array(
						'no_laps' => $driver_laps_data->laps,
						'created_at' => date('Y-m-d H:i:s')
					),$laps_exist[0]['laps_id']);
				}
			}
			$returndata['status'] = TRUE;
			$returndata['message'] = "Driver laps created successfully.";
		}
		else{
			$returndata['status'] = FALSE;
			$returndata['message'] = "Please provide required fields.";
		}
		return $this->apiResponse($returndata);
	}
	
	public function get_drag_race_mode()
	{
		$returndata = array();
		$race_mode = new Race_mode();
		$race_mode_data = $race_mode->crud_read();
		$returndata['race_mode_data'] = $race_mode_data;
		$returndata['status'] = TRUE;
		$returndata['message'] = "Drage race mode fetched successfully.";
		return $this->apiResponse($returndata);
	}
}
?>