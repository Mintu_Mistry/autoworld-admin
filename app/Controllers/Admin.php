<?php 
namespace App\Controllers;

/*-----Load models-----*/
use App\Models\Login;
use App\Models\Content;
use App\Models\Contact;
use App\Models\Helmet_images;
use App\Models\Chassis;
use App\Models\Users;
use App\Models\User_driver;
use App\Models\Driver_course;
use App\Models\Driver_drag_strip;
use App\Models\Driver_laps;
use App\Models\Driver_stats;
use App\Models\Race_type;
use CodeIgniter\HTTP\Files\UploadedFile;
class Admin extends BaseController
{

	public function __construct()
    {
        $this->session = session();
    }

	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){
			header("location: ".base_url('admin')."/login");
		}
		else{
			return;
		}
	}

	private function loadView($viewname, $data = array())
	{
		echo view("admin/admin_header");
		echo view("admin/$viewname", $data);
		echo view("admin/admin_footer");
	}

	private function loadView1($viewname, $data = array())
	{
		echo view("admin/$viewname", $data);
	}

	public function index()
	{
		$this->checkLogin();
		$this->loadView("dashboard");	
	}

	public function credentialsVerify()
	{
		$login = new login();
		$admin_id = $login->admin_verify(
			$this->request->getPost('email'),
			$this->request->getPost('password')
		);
		if ($admin_id != NULL){
			$this->session->set('admin_id', $admin_id);
			$this->session->admin_data = $login->admin_data($admin_id);
		   	return redirect()->to(base_url('admin')); //dashboard
		}
		else{
			$data['msg'] = "Incorrect Email or Password";
			$this->loadView1("login", $data);
		}
	}

	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('admin/login') );
	}


	/*--------------------------Content Management----------------------------*/
	public function editContent($plan_id)
	{
		$this->checkLogin();
		$content = new Content();
		
		$content->crud_update(
			array(
				"page_name" => $this->request->getPost("page_name"),
				"page_content" => $this->request->getPost("page_content")
			), $plan_id
		);
		$this->session->setFlashdata('success_msg', "Content updated successfully");
		return redirect()->to(base_url('admin/content_management'));
	}

	/*-------------------------- Helmet Image Management-------------------*/
	public function addHelmetImage()
	{
		$this->checkLogin();
		$helmet_images = new Helmet_images();
		$helmet_image = $this->request->getFile("helmet_image");
		if($helmet_image){
			if($helmet_image->isValid()){
				$helmet_imagepath = $helmet_image->store();
				if($helmet_imagepath != ""){
					$helmet_images->crud_create(
						array(
							"image_path" => $helmet_imagepath,
							"createdat" => date('Y-m-d H:i:s')
						)
					);
				}
			}
		}
		$this->session->setFlashdata('success_msg', "Helmet Image added successfully");
		return redirect()->to(base_url('admin/helmet_images_management'));
	}

	public function editHelmetImage($helmet_image_id)
	{
		$this->checkLogin();
		$helmet_images = new Helmet_images();
		$helmet_image = $this->request->getFile("helmet_image");
	
		if($helmet_image->isValid()){
			$helmet_imagepath = $helmet_image->store();
			if($helmet_imagepath != ""){
				$helmet_images->crud_update(
					array(
						"image_path" => $helmet_imagepath,
						"createdat" => date('Y-m-d H:i:s')
					), $helmet_image_id
				);
			}
		}
		$this->session->setFlashdata('success_msg', "Helmet Image updated successfully");
		return redirect()->to(base_url('admin/helmet_images_management'));
	}

	public function deleteHelmetImage($helmet_image_id)
	{
		$this->checkLogin();
		$helmet_images = new Helmet_images();
		$helmet_images->crud_delete($helmet_image_id);
		$this->session->setFlashdata('success_msg', "Helmet Image deleted successfully");
		return redirect()->to(base_url('admin/helmet_images_management'));
	}

	/*-------------------------- Chassis Type Management-------------------*/
	public function addChassis()
	{
		$this->checkLogin();
		$chassis = new Chassis();
		$chassis_type = $this->request->getPost("chassis_type");
		$chassis->crud_create(
			array(
				"chassis_type" => $chassis_type,
				"created_at" => date('Y-m-d H:i:s')
			)
		);
		$this->session->setFlashdata('success_msg', "Chassis added successfully");
		return redirect()->to(base_url('admin/chassis_management'));
	}

	public function editChassis($chassis_id)
	{
		$this->checkLogin();
		$chassis = new Chassis();
		$chassis_type = $this->request->getPost("chassis_type");
		$chassis->crud_update(
			array(
				"chassis_type" => $chassis_type,
				"created_at" => date('Y-m-d H:i:s')
			), $chassis_id
		);
		$this->session->setFlashdata('success_msg', "Chassis updated successfully");
		return redirect()->to(base_url('admin/chassis_management'));
	}

	public function deleteChassis($chassis_id)
	{
		$this->checkLogin();
		$chassis = new Chassis();
		$chassis->crud_delete($chassis_id);
		$this->session->setFlashdata('success_msg', "Chassis deleted successfully");
		return redirect()->to(base_url('admin/chassis_management'));
	}


	/*-------------------------- Users Management-------------------------*/
	public function addUser()
	{
		$this->checkLogin();
		$users = new Users();
		$full_name = $this->request->getPost("full_name");
		$email = $this->request->getPost("email");
		$phone_number = $this->request->getPost("phone_number");
		$password = $this->request->getPost("password");
		$track_length = $this->request->getPost("track_length");
		$track_lane = $this->request->getPost("track_lane");
		$profile_image = $this->request->getFile("profile_image");
		if($profile_image){
			if($profile_image->isValid()){
				$profile_imagepath = $profile_image->store();
				if($profile_imagepath != ""){
					$users->crud_create(
						array(
							"full_name" => $full_name,
							"email" => $email,
							"phone_number" => $phone_number,
							"password" => $password,
							"track_lane" => $track_lane,
							"track_length" => $track_length,
							"profile_image" => $profile_imagepath,
							"created_at" => date('Y-m-d H:i:s')
						)
					);
				}
			}
		}
		//echo $users->getLastQuery();die;
		$this->session->setFlashdata('success_msg', "User added successfully");
		return redirect()->to(base_url('admin/users_management'));
	}

	public function editUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$full_name = $this->request->getPost("full_name");
		$email = $this->request->getPost("email");
		$phone_number = $this->request->getPost("phone_number");
		$password = $this->request->getPost("password");
		$track_length = $this->request->getPost("track_length");
		$track_lane = $this->request->getPost("track_lane");
		$profile_image = $this->request->getFile("profile_image");
		if($profile_image){
			if($profile_image->isValid()){
				$profile_imagepath = $profile_image->store();
				if($profile_imagepath != ""){
					$users->crud_update(
						array(
							"full_name" => $full_name,
							"email" => $email,
							"phone_number" => $phone_number,
							"password" => $password,
							"track_lane" => $track_lane,
							"track_length" => $track_length,
							"profile_image" => $profile_imagepath,
							"created_at" => date('Y-m-d H:i:s')
						), $user_id
					);
				}
			}
		}
		else {
			$users->crud_update(
				array(
					"full_name" => $full_name,
					"email" => $email,
					"phone_number" => $phone_number,
					"password" => $password,
					"track_lane" => $track_lane,
					"track_length" => $track_length,
					"created_at" => date('Y-m-d H:i:s')
				), $user_id
			);
		}
		$this->session->setFlashdata('success_msg', "User updated successfully");
		return redirect()->to(base_url('admin/users_management'));
	}

	public function deleteUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$users->crud_delete($user_id);
		$this->session->setFlashdata('success_msg', "User deleted successfully");
		return redirect()->to(base_url('admin/users_management'));
	}

	public function blockUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$res = $users->crud_read($user_id);
		if(!empty($res))
		{
			if($res[0]['block_status'] == 1)
			{
				$users->crud_update(
				array(
					"block_status" => 0,
				), $user_id);
				$this->session->setFlashdata('success_msg', "User unblocked successfully");
				return redirect()->to(base_url('admin/users_management'));
			} else {
				$users->crud_update(
				array(
					"block_status" => 1,
				), $user_id);
				$this->session->setFlashdata('success_msg', "User Blocked successfully");
			return redirect()->to(base_url('admin/users_management'));
			}
		} else
		{
			$this->session->setFlashdata('success_msg', "User not found");
			return redirect()->to(base_url('admin/users_management'));
		}	
	}


	/*----------------------Contact us Management------------------------*/
	public function export_contact_csv()
	{
		$this->checkLogin();
		$contact = new Contact();
		$filename = "contact_query_data.csv";
		$fp = fopen('php://output', 'w');
		$contact_query = $contact->crud_read();
		$header = array(
					"User Name",
					"Email",
					"Message",
					"Date"
				);
		fputcsv($fp, $header);
		foreach ($contact_query as $query) {
			$line =	array(
					"User Name" => $query['user_name'],
					"Email" => $query['email'],
					"Message" => $query['message'],
					"Date" => date('m-d-Y', strtotime($query['created_at']))
				);
			fputcsv($fp, $line);
		}
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename= $filename");
		header("Content-Type: application/csv; ");
		fclose($fp);
	}


	/*--------------------------Load Views----------------------------*/
	public function login()
	{
		echo view("admin/login");
	}

	public function content_management()
	{
		$this->checkLogin();
		$content = new Content();
		$data['content_details'] = $content->crud_read();
		$this->loadView("content", $data);
	}

	public function contact_management()
	{
		$this->checkLogin();
		$contact = new Contact();
		$data['contact_query'] = $contact->crud_read();
		$this->loadView("contact", $data);
	}

	public function helmet_images_management()
	{
		$this->checkLogin();
		$helmet_images = new Helmet_images();
		$data['helmet_image_details'] = $helmet_images->crud_read();
		$this->loadView("helmet_images", $data);
	}

	public function chassis_management()
	{
		$this->checkLogin();
		$chassis = new Chassis();
		$data['chassis_details'] = $chassis->crud_read();
		$this->loadView("chassis_management", $data);
	}

	public function users_management()
	{
		$this->checkLogin();
		$users = new Users();
		$data['users_details'] = $users->crud_read();
		$this->loadView("users_management", $data);
	}

	public function users_driver_management($user_id)
	{
		$this->checkLogin();
		$user_driver = new User_driver();
		$data['user_id'] = $user_id;
		$data['users_driver_details'] = $user_driver->crud_read($user_id);
		$this->loadView("users_driver", $data);
	}

	public function driver_drag_strip_management($driver_id)
	{
		$this->checkLogin();
		$driver_drag_strip = new Driver_drag_strip();
		$data['driver_id'] = $driver_id;
		$data['driver_strip_details'] = $driver_drag_strip->crud_read($driver_id);
		$this->loadView("driver_strip", $data);
	}

	public function driver_roadcourse_management($driver_id)
	{
		$this->checkLogin();
		$driver_course = new Driver_course();
		$data['driver_id'] = $driver_id;
		$data['driver_course_details'] = $driver_course->crud_read($driver_id);
		$this->loadView("driver_course", $data);
	}

	public function driver_stats_management($driver_id)
	{
		$this->checkLogin();
		$driver_stats = new Driver_stats();
		$data['driver_id'] = $driver_id;
		$data['driver_stats_details'] = $driver_stats->crud_read($driver_id);
		$this->loadView("driver_stats", $data);
	}

	public function driver_laps_management($driver_id)
	{
		$this->checkLogin();
		$driver_laps = new Driver_laps();
		$data['driver_id'] = $driver_id;
		$data['driver_laps_details'] = $driver_laps->crud_read($driver_id);
		$this->loadView("driver_laps", $data);
	}

	public function view_driver_details($driver_id)
	{
		$this->checkLogin();
		$race_type = new Race_type();
		$driver_stats = new Driver_stats();
		$data['course_speed'] = $driver_stats->read_driver_speed($driver_id, 1);
		$data['strip_speed'] = $driver_stats->read_driver_speed($driver_id, 2);
		$data['course_time'] = $driver_stats->read_driver_time($driver_id, 1);
		$data['strip_time'] = $driver_stats->read_driver_time($driver_id, 2);
		$data['race_pro_details'] = count($race_type->read_driver_race_type($driver_id, 1));
		$data['race_sport_details'] = count($race_type->read_driver_race_type($driver_id, 2));
		$data['driver_id'] = $driver_id;
		$this->loadView("driver_result_details", $data);
	}

}
